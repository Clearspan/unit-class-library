namespace UnitClassLibrary
{

	public partial class AngularDistance
	{
        public readonly static AngularDistance Zero = 0 * Degree;


		///<summary>Generator method that constructs AngularDistance with assumption that the passed value is in Radians</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static AngularDistance InRadians(double passedValue)
		{
			return new AngularDistance(AngleType.Radian, passedValue);
		}

		///<summary>Generator method that constructs AngularDistance with assumption that the passed value is in Degrees</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static AngularDistance InDegrees(double passedValue)
		{
			return new AngularDistance(AngleType.Degree, passedValue);
		}
	}
}