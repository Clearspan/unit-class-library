﻿namespace UnitClassLibrary
{
    public interface Unit<T>
    {
        T AbsoluteValue();
        T ValueZero();
    }
}
