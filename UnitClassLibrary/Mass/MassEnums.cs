namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a Mass is.</summary>
	public enum MassType { Gram, Kilogram, MetricTon, Milligram, Microgram, LongTon, ShortTon, Stone, Pound, Ounce }
}