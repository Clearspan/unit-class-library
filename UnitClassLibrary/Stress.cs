﻿using System;

#pragma warning disable 1591

namespace UnitClassLibrary
{
    /// <summary>
    /// Force applied to an area
    /// </summary>
    public class Stress :IComparable<Stress>
    {
        #region private fields and constants

        private Force _force;
        private Area _area;

        #endregion

        #region Constructors

        /// <summary>
        /// Zero Constructor
        /// </summary>
        public Stress()
        {
            _area = new Area();
            _force = Force.Zero;
        }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="passedArea"></param>
        /// <param name="passedForce"></param>
        public Stress(Force passedForce, Area passedArea)
        {
            _area = passedArea;
            _force = passedForce;
        }

        /// <summary>
        /// Create a stress object from a composite unit; the value is arbitrarily distributed among the component units.
        /// </summary>
        /// <param name="passedStressType"></param>
        /// <param name="passedValue"></param>
        public Stress(StressType passedStressType, double passedValue)
        {
            switch (passedStressType)
            {
                case StressType.PoundsPerSquareInch:
                    _area = new Area(AreaType.InchesSquared, 1);
                    _force = new Force(ForceType.Pound, passedValue);
                    break;
                case StressType.PoundsPerSquareMillimeter:
                    _area = new Area(AreaType.MillimetersSquared, 1);
                    _force = new Force(ForceType.Pound, passedValue);
                    break;
                case StressType.NewtonsPerSquareMeter:
                    _area = new Area(AreaType.MetersSquared, 1);
                    _force = new Force(ForceType.Newton, passedValue);
                    break;
                case StressType.NewtonsPerSquareMillimeter:
                    _area = new Area(AreaType.MillimetersSquared, 1);
                    _force = new Force(ForceType.Newton, passedValue);
                    break;
                default:
                    // Should never reach; cases should cover all members of enumerated set
                    throw new NotSupportedException("Unit not supported!");
            }
        }

        #endregion

        #region Properties

        public double PoundsPerSquareInch
        {
            get { return _force.Pounds / _area.InchesSquared; }
        }

        public double PoundsPerSquareMillimeter
        {
            get { return _force.Pounds / _area.MillimetersSquared; }
        }

        public double NewtonsPerSquareMeter
        {
            get { return _force.Newtons / _area.MetersSquared; }
        }

        public double NewtonsPerSquareMillimeter
        {
            get { return _force.Newtons / _area.MillimetersSquared; }

        }

        public double GetValue(StressType Units)
        {
            switch (Units)
            {
                case StressType.PoundsPerSquareInch:
                    return PoundsPerSquareInch;
                case StressType.PoundsPerSquareMillimeter:
                    return PoundsPerSquareMillimeter;
                case StressType.NewtonsPerSquareMeter:
                    return NewtonsPerSquareMeter;
                case StressType.NewtonsPerSquareMillimeter:
                    return NewtonsPerSquareMillimeter;
            }
            throw new Exception("Unknown StressType");
        }

        #endregion

        #region Overloaded Operators

        /* You may notice that we do not overload the increment and decrement operators nor do we overload multiplication and division.
         * This is because the user of this library does not know what is being internally stored and those operations will not return useful information. 
         */

        public static Stress operator *(double d, Stress s)
        {
            return new Stress(StressType.PoundsPerSquareInch, s.PoundsPerSquareInch*d);
        }

        public static Stress operator *(Stress s, double d)
        {
            return d * s;
        }

        public static Stress operator /(Stress s, double d)
        {
            return new Stress(StressType.PoundsPerSquareInch, s.PoundsPerSquareInch / d);
        }
        public static Stress operator +(Stress s1, Stress s2)
        {
            //add the two Stresses together
            //return a new Stress with the new value
            //return new Stress( s1._area + s2._area, s1._force + s2._force);
            throw new NotImplementedException("Consult engineer: might not be allowed");
        }

        public static Stress operator -(Stress s1, Stress s2)
        {
            //subtract the two Stresss
            //return a new Stress with the new value
            //return new Stress(s1._area - s2._area, s1._force - s2._force);
            throw new NotImplementedException("Consult engineer: might not be allowed");
        }

        /// <summary>
        /// Not a perfect equality operator, is only accurate up to the Constants.AcceptedEqualityDeviationConstant 
        /// </summary>
        public static bool operator ==(Stress s1, Stress s2)
        {
            if ((object)s1 == null)
            {
                if ((object)s2 == null)
                {
                    return true;
                }
                return false;
            }
            return s1.Equals(s2);
        }

        /// <summary>
        /// Not a perfect inequality operator, is only accurate up to Constants.AcceptedEqualityDeviationConstant 
        /// </summary>
        public static bool operator !=(Stress s1, Stress s2)
        {
            if ((object)s1 == null)
            {
                if ((object)s2 == null)
                {
                    return false;
                }
                return true;
            }
            return !s1.Equals(s2);
        }

        public static bool operator >(Stress s1, Stress s2)
        {
            return (s1.PoundsPerSquareMillimeter > s2.PoundsPerSquareMillimeter);
        }

        public static bool operator <(Stress s1, Stress s2)
        {
            return (s1.PoundsPerSquareMillimeter < s2.PoundsPerSquareMillimeter);
        }

        public static bool operator >=(Stress s1, Stress s2)
        {
            return s1.Equals(s2) || s1.PoundsPerSquareMillimeter > s2.PoundsPerSquareMillimeter;
        }

        public static bool operator <=(Stress s1, Stress s2)
        {
            return s1.Equals(s2) || s1.PoundsPerSquareMillimeter < s2.PoundsPerSquareMillimeter;
        }

        /// <summary>
        /// This override determines how this object is inserted into hashtables.
        /// </summary>
        /// <returns>same hashcode as any double would</returns>
        public override int GetHashCode()
        {
            return PoundsPerSquareMillimeter.GetHashCode();
        }


        public override string ToString()
        {
            return this.PoundsPerSquareInch + " Pounds per Square Inch";
        }

        /// <summary>
        /// does the same thing as == if the passed in object is a Stress
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            try
            {
                Stress other = (Stress)obj;

                return Math.Abs(this._force.GetValue(this._force.InternalUnitType) / this._area.GetValue(this._area.InternalUnitType) - other._force.GetValue(this._force.InternalUnitType) / other._area.GetValue(this._area.InternalUnitType)) <= // This stress and the passed stress (in units of this stress)...
                DeviationDefaults.AcceptedEqualityDeviationStress._force.GetValue(this._force.InternalUnitType) / DeviationDefaults.AcceptedEqualityDeviationStress._area.GetValue(this._area.InternalUnitType); // Is less than the accepted deviation stress constant (0.01%) in units of this stress
            }
            catch
            {
                return false;
            }
        }

        #endregion

        /// <summary>
        /// This implements the IComparable interface and allows Stresss to be sorted and such
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Stress other)
        {
            // The comparison depends on the comparison of  
            // the underlying Double values
            if (this.Equals(other))
                return 0;
            else
                return PoundsPerSquareMillimeter.CompareTo(other.PoundsPerSquareMillimeter);
        }
    }
}
