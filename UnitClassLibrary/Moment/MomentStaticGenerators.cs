namespace UnitClassLibrary
{

	public partial class Moment
	{

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsMillimeters(double passedValue)
		{
			return new Moment(MomentType.NewtonsMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsCentimeters(double passedValue)
		{
			return new Moment(MomentType.NewtonsCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsMeters(double passedValue)
		{
			return new Moment(MomentType.NewtonsMeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsKilometers(double passedValue)
		{
			return new Moment(MomentType.NewtonsKilometer, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsInches(double passedValue)
		{
			return new Moment(MomentType.NewtonsInch, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsFeet(double passedValue)
		{
			return new Moment(MomentType.NewtonsFoot, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsYards(double passedValue)
		{
			return new Moment(MomentType.NewtonsYard, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in NewtonsMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithNewtonsMiles(double passedValue)
		{
			return new Moment(MomentType.NewtonsMile, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsMillimeters(double passedValue)
		{
			return new Moment(MomentType.PoundsMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsCentimeters(double passedValue)
		{
			return new Moment(MomentType.PoundsCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsMeters(double passedValue)
		{
			return new Moment(MomentType.PoundsMeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsKilometers(double passedValue)
		{
			return new Moment(MomentType.PoundsKilometer, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsInches(double passedValue)
		{
			return new Moment(MomentType.PoundsInch, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsFeet(double passedValue)
		{
			return new Moment(MomentType.PoundsFoot, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsYards(double passedValue)
		{
			return new Moment(MomentType.PoundsYard, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in PoundsMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithPoundsMiles(double passedValue)
		{
			return new Moment(MomentType.PoundsMile, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsMillimeters(double passedValue)
		{
			return new Moment(MomentType.KipsMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsCentimeters(double passedValue)
		{
			return new Moment(MomentType.KipsCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsMeters(double passedValue)
		{
			return new Moment(MomentType.KipsMeter, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsKilometers(double passedValue)
		{
			return new Moment(MomentType.KipsKilometer, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsInches(double passedValue)
		{
			return new Moment(MomentType.KipsInch, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsFeet(double passedValue)
		{
			return new Moment(MomentType.KipsFoot, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsYards(double passedValue)
		{
			return new Moment(MomentType.KipsYard, passedValue);
		}

		///<summary>Generator method that constructs Moment with assumption that the passed value is in KipsMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Moment MakeMomentWithKipsMiles(double passedValue)
		{
			return new Moment(MomentType.KipsMile, passedValue);
		}
	}
}