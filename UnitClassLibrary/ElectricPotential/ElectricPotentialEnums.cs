namespace UnitClassLibrary
{
	/// <summary> Enum for specifying the type of unit a ElectricPotential is.</summary>
	public enum ElectricPotentialType { Microvolt, Millivolt, Volt, Kilovolt, Megavolt, Petavolt }
}