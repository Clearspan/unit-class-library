﻿
#pragma warning disable 1591

namespace UnitClassLibrary
{
    /// <summary>
    /// Enum for specifying the type of unit an area is
    /// </summary>
    public enum AreaType { MillimetersSquared, CentimetersSquared, MetersSquared, KilometersSquared, InchesSquared, FeetSquared, YardsSquared, MilesSquared }

    /// <summary>
    /// Enum for specifying the type of unit a power is
    /// </summary>
    public enum PowerType { Watt, Horsepower, FootPoundsPerSecond, MetricHorsepower, ErgsPerSecond }

    /// <summary>
    /// Enum for specifying the type of unit a stress is
    /// </summary>
    public enum StressType { PoundsPerSquareInch, PoundsPerSquareMillimeter, NewtonsPerSquareMeter, NewtonsPerSquareMillimeter }

    /// <summary>
    /// Enum for specifying the type of unit a volume is
    /// </summary>	
	public enum VolumeType { Milliliters, CubicCentimeters, Liters, CubicMeters, CubicThirtySeconds, CubicSixteenths, CubicInches, CubicFeet, CubicYards, CubicMiles, Gallons, Quarts, Pints, Cups, FluidOunces }
	

}
