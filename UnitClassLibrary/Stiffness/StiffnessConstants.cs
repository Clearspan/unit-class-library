namespace UnitClassLibrary
{

	public partial class Stiffness
	{

		public static Stiffness NewtonsPerMillimeter
		{
			get { return new Stiffness(StiffnessType.NewtonsPerMillimeter, 1); }
		}

		public static Stiffness NewtonsPerCentimeter
		{
			get { return new Stiffness(StiffnessType.NewtonsPerCentimeter, 1); }
		}

		public static Stiffness NewtonsPerMeter
		{
			get { return new Stiffness(StiffnessType.NewtonsPerMeter, 1); }
		}

		public static Stiffness NewtonsPerKilometer
		{
			get { return new Stiffness(StiffnessType.NewtonsPerKilometer, 1); }
		}

		public static Stiffness NewtonsPerInch
		{
			get { return new Stiffness(StiffnessType.NewtonsPerInch, 1); }
		}

		public static Stiffness NewtonsPerFoot
		{
			get { return new Stiffness(StiffnessType.NewtonsPerFoot, 1); }
		}

		public static Stiffness NewtonsPerYard
		{
			get { return new Stiffness(StiffnessType.NewtonsPerYard, 1); }
		}

		public static Stiffness NewtonsPerMile
		{
			get { return new Stiffness(StiffnessType.NewtonsPerMile, 1); }
		}

		public static Stiffness PoundsPerMillimeter
		{
			get { return new Stiffness(StiffnessType.PoundsPerMillimeter, 1); }
		}

		public static Stiffness PoundsPerCentimeter
		{
			get { return new Stiffness(StiffnessType.PoundsPerCentimeter, 1); }
		}

		public static Stiffness PoundsPerMeter
		{
			get { return new Stiffness(StiffnessType.PoundsPerMeter, 1); }
		}

		public static Stiffness PoundsPerKilometer
		{
			get { return new Stiffness(StiffnessType.PoundsPerKilometer, 1); }
		}

		public static Stiffness PoundsPerInch
		{
			get { return new Stiffness(StiffnessType.PoundsPerInch, 1); }
		}

		public static Stiffness PoundsPerFoot
		{
			get { return new Stiffness(StiffnessType.PoundsPerFoot, 1); }
		}

		public static Stiffness PoundsPerYard
		{
			get { return new Stiffness(StiffnessType.PoundsPerYard, 1); }
		}

		public static Stiffness PoundsPerMile
		{
			get { return new Stiffness(StiffnessType.PoundsPerMile, 1); }
		}

		public static Stiffness KipsPerMillimeter
		{
			get { return new Stiffness(StiffnessType.KipsPerMillimeter, 1); }
		}

		public static Stiffness KipsPerCentimeter
		{
			get { return new Stiffness(StiffnessType.KipsPerCentimeter, 1); }
		}

		public static Stiffness KipsPerMeter
		{
			get { return new Stiffness(StiffnessType.KipsPerMeter, 1); }
		}

		public static Stiffness KipsPerKilometer
		{
			get { return new Stiffness(StiffnessType.KipsPerKilometer, 1); }
		}

		public static Stiffness KipsPerInch
		{
			get { return new Stiffness(StiffnessType.KipsPerInch, 1); }
		}

		public static Stiffness KipsPerFoot
		{
			get { return new Stiffness(StiffnessType.KipsPerFoot, 1); }
		}

		public static Stiffness KipsPerYard
		{
			get { return new Stiffness(StiffnessType.KipsPerYard, 1); }
		}

		public static Stiffness KipsPerMile
		{
			get { return new Stiffness(StiffnessType.KipsPerMile, 1); }
		}
	}
}