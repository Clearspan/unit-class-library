namespace UnitClassLibrary
{

	public partial class Stiffness
	{

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerMillimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerCentimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerMeters(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerMeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerKilometers(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerKilometer, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerInches(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerInch, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerFeet(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerFoot, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerYards(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerYard, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in NewtonsPerMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithNewtonsPerMiles(double passedValue)
		{
			return new Stiffness(StiffnessType.NewtonsPerMile, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerMillimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerCentimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerMeters(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerMeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerKilometers(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerKilometer, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerInches(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerInch, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerFeet(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerFoot, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerYards(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerYard, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in PoundsPerMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithPoundsPerMiles(double passedValue)
		{
			return new Stiffness(StiffnessType.PoundsPerMile, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerMillimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerMillimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerMillimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerCentimeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerCentimeters(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerCentimeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerMeters</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerMeters(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerMeter, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerKilometers</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerKilometers(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerKilometer, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerInches</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerInches(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerInch, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerFeet</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerFeet(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerFoot, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerYards</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerYards(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerYard, passedValue);
		}

		///<summary>Generator method that constructs Stiffness with assumption that the passed value is in KipsPerMiles</summary>
		///<param name="passedValue"></param>
		///<returns></returns>
		public static Stiffness MakeStiffnessWithKipsPerMiles(double passedValue)
		{
			return new Stiffness(StiffnessType.KipsPerMile, passedValue);
		}
	}
}