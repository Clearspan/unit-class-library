namespace UnitClassLibrary
{

	public partial class Speed
	{

		public static Speed MillimetersPerNanosecond
		{
			get { return new Speed(SpeedType.MillimetersPerNanosecond, 1); }
		}

		public static Speed MillimetersPerMicrosecond
		{
			get { return new Speed(SpeedType.MillimetersPerMicrosecond, 1); }
		}

		public static Speed MillimetersPerMillisecond
		{
			get { return new Speed(SpeedType.MillimetersPerMillisecond, 1); }
		}

		public static Speed MillimetersPerSecond
		{
			get { return new Speed(SpeedType.MillimetersPerSecond, 1); }
		}

		public static Speed MillimetersPerMinute
		{
			get { return new Speed(SpeedType.MillimetersPerMinute, 1); }
		}

		public static Speed MillimetersPerHour
		{
			get { return new Speed(SpeedType.MillimetersPerHour, 1); }
		}

		public static Speed MillimetersPerDay
		{
			get { return new Speed(SpeedType.MillimetersPerDay, 1); }
		}

		public static Speed MillimetersPerWeek
		{
			get { return new Speed(SpeedType.MillimetersPerWeek, 1); }
		}

		public static Speed MillimetersPerMonth
		{
			get { return new Speed(SpeedType.MillimetersPerMonth, 1); }
		}

		public static Speed MillimetersPerYear
		{
			get { return new Speed(SpeedType.MillimetersPerYear, 1); }
		}

		public static Speed MillimetersPerDecade
		{
			get { return new Speed(SpeedType.MillimetersPerDecade, 1); }
		}

		public static Speed MillimetersPerCentury
		{
			get { return new Speed(SpeedType.MillimetersPerCentury, 1); }
		}

		public static Speed CentimetersPerNanosecond
		{
			get { return new Speed(SpeedType.CentimetersPerNanosecond, 1); }
		}

		public static Speed CentimetersPerMicrosecond
		{
			get { return new Speed(SpeedType.CentimetersPerMicrosecond, 1); }
		}

		public static Speed CentimetersPerMillisecond
		{
			get { return new Speed(SpeedType.CentimetersPerMillisecond, 1); }
		}

		public static Speed CentimetersPerSecond
		{
			get { return new Speed(SpeedType.CentimetersPerSecond, 1); }
		}

		public static Speed CentimetersPerMinute
		{
			get { return new Speed(SpeedType.CentimetersPerMinute, 1); }
		}

		public static Speed CentimetersPerHour
		{
			get { return new Speed(SpeedType.CentimetersPerHour, 1); }
		}

		public static Speed CentimetersPerDay
		{
			get { return new Speed(SpeedType.CentimetersPerDay, 1); }
		}

		public static Speed CentimetersPerWeek
		{
			get { return new Speed(SpeedType.CentimetersPerWeek, 1); }
		}

		public static Speed CentimetersPerMonth
		{
			get { return new Speed(SpeedType.CentimetersPerMonth, 1); }
		}

		public static Speed CentimetersPerYear
		{
			get { return new Speed(SpeedType.CentimetersPerYear, 1); }
		}

		public static Speed CentimetersPerDecade
		{
			get { return new Speed(SpeedType.CentimetersPerDecade, 1); }
		}

		public static Speed CentimetersPerCentury
		{
			get { return new Speed(SpeedType.CentimetersPerCentury, 1); }
		}

		public static Speed MetersPerNanosecond
		{
			get { return new Speed(SpeedType.MetersPerNanosecond, 1); }
		}

		public static Speed MetersPerMicrosecond
		{
			get { return new Speed(SpeedType.MetersPerMicrosecond, 1); }
		}

		public static Speed MetersPerMillisecond
		{
			get { return new Speed(SpeedType.MetersPerMillisecond, 1); }
		}

		public static Speed MetersPerSecond
		{
			get { return new Speed(SpeedType.MetersPerSecond, 1); }
		}

		public static Speed MetersPerMinute
		{
			get { return new Speed(SpeedType.MetersPerMinute, 1); }
		}

		public static Speed MetersPerHour
		{
			get { return new Speed(SpeedType.MetersPerHour, 1); }
		}

		public static Speed MetersPerDay
		{
			get { return new Speed(SpeedType.MetersPerDay, 1); }
		}

		public static Speed MetersPerWeek
		{
			get { return new Speed(SpeedType.MetersPerWeek, 1); }
		}

		public static Speed MetersPerMonth
		{
			get { return new Speed(SpeedType.MetersPerMonth, 1); }
		}

		public static Speed MetersPerYear
		{
			get { return new Speed(SpeedType.MetersPerYear, 1); }
		}

		public static Speed MetersPerDecade
		{
			get { return new Speed(SpeedType.MetersPerDecade, 1); }
		}

		public static Speed MetersPerCentury
		{
			get { return new Speed(SpeedType.MetersPerCentury, 1); }
		}

		public static Speed KilometersPerNanosecond
		{
			get { return new Speed(SpeedType.KilometersPerNanosecond, 1); }
		}

		public static Speed KilometersPerMicrosecond
		{
			get { return new Speed(SpeedType.KilometersPerMicrosecond, 1); }
		}

		public static Speed KilometersPerMillisecond
		{
			get { return new Speed(SpeedType.KilometersPerMillisecond, 1); }
		}

		public static Speed KilometersPerSecond
		{
			get { return new Speed(SpeedType.KilometersPerSecond, 1); }
		}

		public static Speed KilometersPerMinute
		{
			get { return new Speed(SpeedType.KilometersPerMinute, 1); }
		}

		public static Speed KilometersPerHour
		{
			get { return new Speed(SpeedType.KilometersPerHour, 1); }
		}

		public static Speed KilometersPerDay
		{
			get { return new Speed(SpeedType.KilometersPerDay, 1); }
		}

		public static Speed KilometersPerWeek
		{
			get { return new Speed(SpeedType.KilometersPerWeek, 1); }
		}

		public static Speed KilometersPerMonth
		{
			get { return new Speed(SpeedType.KilometersPerMonth, 1); }
		}

		public static Speed KilometersPerYear
		{
			get { return new Speed(SpeedType.KilometersPerYear, 1); }
		}

		public static Speed KilometersPerDecade
		{
			get { return new Speed(SpeedType.KilometersPerDecade, 1); }
		}

		public static Speed KilometersPerCentury
		{
			get { return new Speed(SpeedType.KilometersPerCentury, 1); }
		}

		public static Speed InchesPerNanosecond
		{
			get { return new Speed(SpeedType.InchesPerNanosecond, 1); }
		}

		public static Speed InchesPerMicrosecond
		{
			get { return new Speed(SpeedType.InchesPerMicrosecond, 1); }
		}

		public static Speed InchesPerMillisecond
		{
			get { return new Speed(SpeedType.InchesPerMillisecond, 1); }
		}

		public static Speed InchesPerSecond
		{
			get { return new Speed(SpeedType.InchesPerSecond, 1); }
		}

		public static Speed InchesPerMinute
		{
			get { return new Speed(SpeedType.InchesPerMinute, 1); }
		}

		public static Speed InchesPerHour
		{
			get { return new Speed(SpeedType.InchesPerHour, 1); }
		}

		public static Speed InchesPerDay
		{
			get { return new Speed(SpeedType.InchesPerDay, 1); }
		}

		public static Speed InchesPerWeek
		{
			get { return new Speed(SpeedType.InchesPerWeek, 1); }
		}

		public static Speed InchesPerMonth
		{
			get { return new Speed(SpeedType.InchesPerMonth, 1); }
		}

		public static Speed InchesPerYear
		{
			get { return new Speed(SpeedType.InchesPerYear, 1); }
		}

		public static Speed InchesPerDecade
		{
			get { return new Speed(SpeedType.InchesPerDecade, 1); }
		}

		public static Speed InchesPerCentury
		{
			get { return new Speed(SpeedType.InchesPerCentury, 1); }
		}

		public static Speed FeetPerNanosecond
		{
			get { return new Speed(SpeedType.FeetPerNanosecond, 1); }
		}

		public static Speed FeetPerMicrosecond
		{
			get { return new Speed(SpeedType.FeetPerMicrosecond, 1); }
		}

		public static Speed FeetPerMillisecond
		{
			get { return new Speed(SpeedType.FeetPerMillisecond, 1); }
		}

		public static Speed FeetPerSecond
		{
			get { return new Speed(SpeedType.FeetPerSecond, 1); }
		}

		public static Speed FeetPerMinute
		{
			get { return new Speed(SpeedType.FeetPerMinute, 1); }
		}

		public static Speed FeetPerHour
		{
			get { return new Speed(SpeedType.FeetPerHour, 1); }
		}

		public static Speed FeetPerDay
		{
			get { return new Speed(SpeedType.FeetPerDay, 1); }
		}

		public static Speed FeetPerWeek
		{
			get { return new Speed(SpeedType.FeetPerWeek, 1); }
		}

		public static Speed FeetPerMonth
		{
			get { return new Speed(SpeedType.FeetPerMonth, 1); }
		}

		public static Speed FeetPerYear
		{
			get { return new Speed(SpeedType.FeetPerYear, 1); }
		}

		public static Speed FeetPerDecade
		{
			get { return new Speed(SpeedType.FeetPerDecade, 1); }
		}

		public static Speed FeetPerCentury
		{
			get { return new Speed(SpeedType.FeetPerCentury, 1); }
		}

		public static Speed YardsPerNanosecond
		{
			get { return new Speed(SpeedType.YardsPerNanosecond, 1); }
		}

		public static Speed YardsPerMicrosecond
		{
			get { return new Speed(SpeedType.YardsPerMicrosecond, 1); }
		}

		public static Speed YardsPerMillisecond
		{
			get { return new Speed(SpeedType.YardsPerMillisecond, 1); }
		}

		public static Speed YardsPerSecond
		{
			get { return new Speed(SpeedType.YardsPerSecond, 1); }
		}

		public static Speed YardsPerMinute
		{
			get { return new Speed(SpeedType.YardsPerMinute, 1); }
		}

		public static Speed YardsPerHour
		{
			get { return new Speed(SpeedType.YardsPerHour, 1); }
		}

		public static Speed YardsPerDay
		{
			get { return new Speed(SpeedType.YardsPerDay, 1); }
		}

		public static Speed YardsPerWeek
		{
			get { return new Speed(SpeedType.YardsPerWeek, 1); }
		}

		public static Speed YardsPerMonth
		{
			get { return new Speed(SpeedType.YardsPerMonth, 1); }
		}

		public static Speed YardsPerYear
		{
			get { return new Speed(SpeedType.YardsPerYear, 1); }
		}

		public static Speed YardsPerDecade
		{
			get { return new Speed(SpeedType.YardsPerDecade, 1); }
		}

		public static Speed YardsPerCentury
		{
			get { return new Speed(SpeedType.YardsPerCentury, 1); }
		}

		public static Speed MilesPerNanosecond
		{
			get { return new Speed(SpeedType.MilesPerNanosecond, 1); }
		}

		public static Speed MilesPerMicrosecond
		{
			get { return new Speed(SpeedType.MilesPerMicrosecond, 1); }
		}

		public static Speed MilesPerMillisecond
		{
			get { return new Speed(SpeedType.MilesPerMillisecond, 1); }
		}

		public static Speed MilesPerSecond
		{
			get { return new Speed(SpeedType.MilesPerSecond, 1); }
		}

		public static Speed MilesPerMinute
		{
			get { return new Speed(SpeedType.MilesPerMinute, 1); }
		}

		public static Speed MilesPerHour
		{
			get { return new Speed(SpeedType.MilesPerHour, 1); }
		}

		public static Speed MilesPerDay
		{
			get { return new Speed(SpeedType.MilesPerDay, 1); }
		}

		public static Speed MilesPerWeek
		{
			get { return new Speed(SpeedType.MilesPerWeek, 1); }
		}

		public static Speed MilesPerMonth
		{
			get { return new Speed(SpeedType.MilesPerMonth, 1); }
		}

		public static Speed MilesPerYear
		{
			get { return new Speed(SpeedType.MilesPerYear, 1); }
		}

		public static Speed MilesPerDecade
		{
			get { return new Speed(SpeedType.MilesPerDecade, 1); }
		}

		public static Speed MilesPerCentury
		{
			get { return new Speed(SpeedType.MilesPerCentury, 1); }
		}
	}
}