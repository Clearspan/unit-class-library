using System;

namespace UnitClassLibrary
{

	public partial class DataTransferRate
	{
		public double BitsPerNanoseconds
		{
			get { return _data.Bits / _time.Nanoseconds; }
		}
		public double BitsPerMicroseconds
		{
			get { return _data.Bits / _time.Microseconds; }
		}
		public double BitsPerMilliseconds
		{
			get { return _data.Bits / _time.Milliseconds; }
		}
		public double BitsPerSeconds
		{
			get { return _data.Bits / _time.Seconds; }
		}
		public double BitsPerMinutes
		{
			get { return _data.Bits / _time.Minutes; }
		}
		public double BitsPerHours
		{
			get { return _data.Bits / _time.Hours; }
		}
		public double BitsPerDays
		{
			get { return _data.Bits / _time.Days; }
		}
		public double BitsPerWeeks
		{
			get { return _data.Bits / _time.Weeks; }
		}
		public double BitsPerMonths
		{
			get { return _data.Bits / _time.Months; }
		}
		public double BitsPerYears
		{
			get { return _data.Bits / _time.Years; }
		}
		public double BitsPerDecades
		{
			get { return _data.Bits / _time.Decades; }
		}
		public double BitsPerCenturies
		{
			get { return _data.Bits / _time.Centuries; }
		}
		public double BytesPerNanoseconds
		{
			get { return _data.Bytes / _time.Nanoseconds; }
		}
		public double BytesPerMicroseconds
		{
			get { return _data.Bytes / _time.Microseconds; }
		}
		public double BytesPerMilliseconds
		{
			get { return _data.Bytes / _time.Milliseconds; }
		}
		public double BytesPerSeconds
		{
			get { return _data.Bytes / _time.Seconds; }
		}
		public double BytesPerMinutes
		{
			get { return _data.Bytes / _time.Minutes; }
		}
		public double BytesPerHours
		{
			get { return _data.Bytes / _time.Hours; }
		}
		public double BytesPerDays
		{
			get { return _data.Bytes / _time.Days; }
		}
		public double BytesPerWeeks
		{
			get { return _data.Bytes / _time.Weeks; }
		}
		public double BytesPerMonths
		{
			get { return _data.Bytes / _time.Months; }
		}
		public double BytesPerYears
		{
			get { return _data.Bytes / _time.Years; }
		}
		public double BytesPerDecades
		{
			get { return _data.Bytes / _time.Decades; }
		}
		public double BytesPerCenturies
		{
			get { return _data.Bytes / _time.Centuries; }
		}
		public double KilobytesPerNanoseconds
		{
			get { return _data.Kilobytes / _time.Nanoseconds; }
		}
		public double KilobytesPerMicroseconds
		{
			get { return _data.Kilobytes / _time.Microseconds; }
		}
		public double KilobytesPerMilliseconds
		{
			get { return _data.Kilobytes / _time.Milliseconds; }
		}
		public double KilobytesPerSeconds
		{
			get { return _data.Kilobytes / _time.Seconds; }
		}
		public double KilobytesPerMinutes
		{
			get { return _data.Kilobytes / _time.Minutes; }
		}
		public double KilobytesPerHours
		{
			get { return _data.Kilobytes / _time.Hours; }
		}
		public double KilobytesPerDays
		{
			get { return _data.Kilobytes / _time.Days; }
		}
		public double KilobytesPerWeeks
		{
			get { return _data.Kilobytes / _time.Weeks; }
		}
		public double KilobytesPerMonths
		{
			get { return _data.Kilobytes / _time.Months; }
		}
		public double KilobytesPerYears
		{
			get { return _data.Kilobytes / _time.Years; }
		}
		public double KilobytesPerDecades
		{
			get { return _data.Kilobytes / _time.Decades; }
		}
		public double KilobytesPerCenturies
		{
			get { return _data.Kilobytes / _time.Centuries; }
		}
		public double MegabytesPerNanoseconds
		{
			get { return _data.Megabytes / _time.Nanoseconds; }
		}
		public double MegabytesPerMicroseconds
		{
			get { return _data.Megabytes / _time.Microseconds; }
		}
		public double MegabytesPerMilliseconds
		{
			get { return _data.Megabytes / _time.Milliseconds; }
		}
		public double MegabytesPerSeconds
		{
			get { return _data.Megabytes / _time.Seconds; }
		}
		public double MegabytesPerMinutes
		{
			get { return _data.Megabytes / _time.Minutes; }
		}
		public double MegabytesPerHours
		{
			get { return _data.Megabytes / _time.Hours; }
		}
		public double MegabytesPerDays
		{
			get { return _data.Megabytes / _time.Days; }
		}
		public double MegabytesPerWeeks
		{
			get { return _data.Megabytes / _time.Weeks; }
		}
		public double MegabytesPerMonths
		{
			get { return _data.Megabytes / _time.Months; }
		}
		public double MegabytesPerYears
		{
			get { return _data.Megabytes / _time.Years; }
		}
		public double MegabytesPerDecades
		{
			get { return _data.Megabytes / _time.Decades; }
		}
		public double MegabytesPerCenturies
		{
			get { return _data.Megabytes / _time.Centuries; }
		}
		public double GigabytesPerNanoseconds
		{
			get { return _data.Gigabytes / _time.Nanoseconds; }
		}
		public double GigabytesPerMicroseconds
		{
			get { return _data.Gigabytes / _time.Microseconds; }
		}
		public double GigabytesPerMilliseconds
		{
			get { return _data.Gigabytes / _time.Milliseconds; }
		}
		public double GigabytesPerSeconds
		{
			get { return _data.Gigabytes / _time.Seconds; }
		}
		public double GigabytesPerMinutes
		{
			get { return _data.Gigabytes / _time.Minutes; }
		}
		public double GigabytesPerHours
		{
			get { return _data.Gigabytes / _time.Hours; }
		}
		public double GigabytesPerDays
		{
			get { return _data.Gigabytes / _time.Days; }
		}
		public double GigabytesPerWeeks
		{
			get { return _data.Gigabytes / _time.Weeks; }
		}
		public double GigabytesPerMonths
		{
			get { return _data.Gigabytes / _time.Months; }
		}
		public double GigabytesPerYears
		{
			get { return _data.Gigabytes / _time.Years; }
		}
		public double GigabytesPerDecades
		{
			get { return _data.Gigabytes / _time.Decades; }
		}
		public double GigabytesPerCenturies
		{
			get { return _data.Gigabytes / _time.Centuries; }
		}
		public double TerabytesPerNanoseconds
		{
			get { return _data.Terabytes / _time.Nanoseconds; }
		}
		public double TerabytesPerMicroseconds
		{
			get { return _data.Terabytes / _time.Microseconds; }
		}
		public double TerabytesPerMilliseconds
		{
			get { return _data.Terabytes / _time.Milliseconds; }
		}
		public double TerabytesPerSeconds
		{
			get { return _data.Terabytes / _time.Seconds; }
		}
		public double TerabytesPerMinutes
		{
			get { return _data.Terabytes / _time.Minutes; }
		}
		public double TerabytesPerHours
		{
			get { return _data.Terabytes / _time.Hours; }
		}
		public double TerabytesPerDays
		{
			get { return _data.Terabytes / _time.Days; }
		}
		public double TerabytesPerWeeks
		{
			get { return _data.Terabytes / _time.Weeks; }
		}
		public double TerabytesPerMonths
		{
			get { return _data.Terabytes / _time.Months; }
		}
		public double TerabytesPerYears
		{
			get { return _data.Terabytes / _time.Years; }
		}
		public double TerabytesPerDecades
		{
			get { return _data.Terabytes / _time.Decades; }
		}
		public double TerabytesPerCenturies
		{
			get { return _data.Terabytes / _time.Centuries; }
		}
		public double PetabytesPerNanoseconds
		{
			get { return _data.Petabytes / _time.Nanoseconds; }
		}
		public double PetabytesPerMicroseconds
		{
			get { return _data.Petabytes / _time.Microseconds; }
		}
		public double PetabytesPerMilliseconds
		{
			get { return _data.Petabytes / _time.Milliseconds; }
		}
		public double PetabytesPerSeconds
		{
			get { return _data.Petabytes / _time.Seconds; }
		}
		public double PetabytesPerMinutes
		{
			get { return _data.Petabytes / _time.Minutes; }
		}
		public double PetabytesPerHours
		{
			get { return _data.Petabytes / _time.Hours; }
		}
		public double PetabytesPerDays
		{
			get { return _data.Petabytes / _time.Days; }
		}
		public double PetabytesPerWeeks
		{
			get { return _data.Petabytes / _time.Weeks; }
		}
		public double PetabytesPerMonths
		{
			get { return _data.Petabytes / _time.Months; }
		}
		public double PetabytesPerYears
		{
			get { return _data.Petabytes / _time.Years; }
		}
		public double PetabytesPerDecades
		{
			get { return _data.Petabytes / _time.Decades; }
		}
		public double PetabytesPerCenturies
		{
			get { return _data.Petabytes / _time.Centuries; }
		}
		public double ExabytesPerNanoseconds
		{
			get { return _data.Exabytes / _time.Nanoseconds; }
		}
		public double ExabytesPerMicroseconds
		{
			get { return _data.Exabytes / _time.Microseconds; }
		}
		public double ExabytesPerMilliseconds
		{
			get { return _data.Exabytes / _time.Milliseconds; }
		}
		public double ExabytesPerSeconds
		{
			get { return _data.Exabytes / _time.Seconds; }
		}
		public double ExabytesPerMinutes
		{
			get { return _data.Exabytes / _time.Minutes; }
		}
		public double ExabytesPerHours
		{
			get { return _data.Exabytes / _time.Hours; }
		}
		public double ExabytesPerDays
		{
			get { return _data.Exabytes / _time.Days; }
		}
		public double ExabytesPerWeeks
		{
			get { return _data.Exabytes / _time.Weeks; }
		}
		public double ExabytesPerMonths
		{
			get { return _data.Exabytes / _time.Months; }
		}
		public double ExabytesPerYears
		{
			get { return _data.Exabytes / _time.Years; }
		}
		public double ExabytesPerDecades
		{
			get { return _data.Exabytes / _time.Decades; }
		}
		public double ExabytesPerCenturies
		{
			get { return _data.Exabytes / _time.Centuries; }
		}
		public double ZettabytesPerNanoseconds
		{
			get { return _data.Zettabytes / _time.Nanoseconds; }
		}
		public double ZettabytesPerMicroseconds
		{
			get { return _data.Zettabytes / _time.Microseconds; }
		}
		public double ZettabytesPerMilliseconds
		{
			get { return _data.Zettabytes / _time.Milliseconds; }
		}
		public double ZettabytesPerSeconds
		{
			get { return _data.Zettabytes / _time.Seconds; }
		}
		public double ZettabytesPerMinutes
		{
			get { return _data.Zettabytes / _time.Minutes; }
		}
		public double ZettabytesPerHours
		{
			get { return _data.Zettabytes / _time.Hours; }
		}
		public double ZettabytesPerDays
		{
			get { return _data.Zettabytes / _time.Days; }
		}
		public double ZettabytesPerWeeks
		{
			get { return _data.Zettabytes / _time.Weeks; }
		}
		public double ZettabytesPerMonths
		{
			get { return _data.Zettabytes / _time.Months; }
		}
		public double ZettabytesPerYears
		{
			get { return _data.Zettabytes / _time.Years; }
		}
		public double ZettabytesPerDecades
		{
			get { return _data.Zettabytes / _time.Decades; }
		}
		public double ZettabytesPerCenturies
		{
			get { return _data.Zettabytes / _time.Centuries; }
		}
		public double YottabytesPerNanoseconds
		{
			get { return _data.Yottabytes / _time.Nanoseconds; }
		}
		public double YottabytesPerMicroseconds
		{
			get { return _data.Yottabytes / _time.Microseconds; }
		}
		public double YottabytesPerMilliseconds
		{
			get { return _data.Yottabytes / _time.Milliseconds; }
		}
		public double YottabytesPerSeconds
		{
			get { return _data.Yottabytes / _time.Seconds; }
		}
		public double YottabytesPerMinutes
		{
			get { return _data.Yottabytes / _time.Minutes; }
		}
		public double YottabytesPerHours
		{
			get { return _data.Yottabytes / _time.Hours; }
		}
		public double YottabytesPerDays
		{
			get { return _data.Yottabytes / _time.Days; }
		}
		public double YottabytesPerWeeks
		{
			get { return _data.Yottabytes / _time.Weeks; }
		}
		public double YottabytesPerMonths
		{
			get { return _data.Yottabytes / _time.Months; }
		}
		public double YottabytesPerYears
		{
			get { return _data.Yottabytes / _time.Years; }
		}
		public double YottabytesPerDecades
		{
			get { return _data.Yottabytes / _time.Decades; }
		}
		public double YottabytesPerCenturies
		{
			get { return _data.Yottabytes / _time.Centuries; }
		}

		public double GetValue(DataTransferRateType Units)
		{
			switch (Units)
			{
				case DataTransferRateType.BitsPerNanosecond:
					return BitsPerNanoseconds;
				case DataTransferRateType.BitsPerMicrosecond:
					return BitsPerMicroseconds;
				case DataTransferRateType.BitsPerMillisecond:
					return BitsPerMilliseconds;
				case DataTransferRateType.BitsPerSecond:
					return BitsPerSeconds;
				case DataTransferRateType.BitsPerMinute:
					return BitsPerMinutes;
				case DataTransferRateType.BitsPerHour:
					return BitsPerHours;
				case DataTransferRateType.BitsPerDay:
					return BitsPerDays;
				case DataTransferRateType.BitsPerWeek:
					return BitsPerWeeks;
				case DataTransferRateType.BitsPerMonth:
					return BitsPerMonths;
				case DataTransferRateType.BitsPerYear:
					return BitsPerYears;
				case DataTransferRateType.BitsPerDecade:
					return BitsPerDecades;
				case DataTransferRateType.BitsPerCentury:
					return BitsPerCenturies;
				case DataTransferRateType.BytesPerNanosecond:
					return BytesPerNanoseconds;
				case DataTransferRateType.BytesPerMicrosecond:
					return BytesPerMicroseconds;
				case DataTransferRateType.BytesPerMillisecond:
					return BytesPerMilliseconds;
				case DataTransferRateType.BytesPerSecond:
					return BytesPerSeconds;
				case DataTransferRateType.BytesPerMinute:
					return BytesPerMinutes;
				case DataTransferRateType.BytesPerHour:
					return BytesPerHours;
				case DataTransferRateType.BytesPerDay:
					return BytesPerDays;
				case DataTransferRateType.BytesPerWeek:
					return BytesPerWeeks;
				case DataTransferRateType.BytesPerMonth:
					return BytesPerMonths;
				case DataTransferRateType.BytesPerYear:
					return BytesPerYears;
				case DataTransferRateType.BytesPerDecade:
					return BytesPerDecades;
				case DataTransferRateType.BytesPerCentury:
					return BytesPerCenturies;
				case DataTransferRateType.KilobytesPerNanosecond:
					return KilobytesPerNanoseconds;
				case DataTransferRateType.KilobytesPerMicrosecond:
					return KilobytesPerMicroseconds;
				case DataTransferRateType.KilobytesPerMillisecond:
					return KilobytesPerMilliseconds;
				case DataTransferRateType.KilobytesPerSecond:
					return KilobytesPerSeconds;
				case DataTransferRateType.KilobytesPerMinute:
					return KilobytesPerMinutes;
				case DataTransferRateType.KilobytesPerHour:
					return KilobytesPerHours;
				case DataTransferRateType.KilobytesPerDay:
					return KilobytesPerDays;
				case DataTransferRateType.KilobytesPerWeek:
					return KilobytesPerWeeks;
				case DataTransferRateType.KilobytesPerMonth:
					return KilobytesPerMonths;
				case DataTransferRateType.KilobytesPerYear:
					return KilobytesPerYears;
				case DataTransferRateType.KilobytesPerDecade:
					return KilobytesPerDecades;
				case DataTransferRateType.KilobytesPerCentury:
					return KilobytesPerCenturies;
				case DataTransferRateType.MegabytesPerNanosecond:
					return MegabytesPerNanoseconds;
				case DataTransferRateType.MegabytesPerMicrosecond:
					return MegabytesPerMicroseconds;
				case DataTransferRateType.MegabytesPerMillisecond:
					return MegabytesPerMilliseconds;
				case DataTransferRateType.MegabytesPerSecond:
					return MegabytesPerSeconds;
				case DataTransferRateType.MegabytesPerMinute:
					return MegabytesPerMinutes;
				case DataTransferRateType.MegabytesPerHour:
					return MegabytesPerHours;
				case DataTransferRateType.MegabytesPerDay:
					return MegabytesPerDays;
				case DataTransferRateType.MegabytesPerWeek:
					return MegabytesPerWeeks;
				case DataTransferRateType.MegabytesPerMonth:
					return MegabytesPerMonths;
				case DataTransferRateType.MegabytesPerYear:
					return MegabytesPerYears;
				case DataTransferRateType.MegabytesPerDecade:
					return MegabytesPerDecades;
				case DataTransferRateType.MegabytesPerCentury:
					return MegabytesPerCenturies;
				case DataTransferRateType.GigabytesPerNanosecond:
					return GigabytesPerNanoseconds;
				case DataTransferRateType.GigabytesPerMicrosecond:
					return GigabytesPerMicroseconds;
				case DataTransferRateType.GigabytesPerMillisecond:
					return GigabytesPerMilliseconds;
				case DataTransferRateType.GigabytesPerSecond:
					return GigabytesPerSeconds;
				case DataTransferRateType.GigabytesPerMinute:
					return GigabytesPerMinutes;
				case DataTransferRateType.GigabytesPerHour:
					return GigabytesPerHours;
				case DataTransferRateType.GigabytesPerDay:
					return GigabytesPerDays;
				case DataTransferRateType.GigabytesPerWeek:
					return GigabytesPerWeeks;
				case DataTransferRateType.GigabytesPerMonth:
					return GigabytesPerMonths;
				case DataTransferRateType.GigabytesPerYear:
					return GigabytesPerYears;
				case DataTransferRateType.GigabytesPerDecade:
					return GigabytesPerDecades;
				case DataTransferRateType.GigabytesPerCentury:
					return GigabytesPerCenturies;
				case DataTransferRateType.TerabytesPerNanosecond:
					return TerabytesPerNanoseconds;
				case DataTransferRateType.TerabytesPerMicrosecond:
					return TerabytesPerMicroseconds;
				case DataTransferRateType.TerabytesPerMillisecond:
					return TerabytesPerMilliseconds;
				case DataTransferRateType.TerabytesPerSecond:
					return TerabytesPerSeconds;
				case DataTransferRateType.TerabytesPerMinute:
					return TerabytesPerMinutes;
				case DataTransferRateType.TerabytesPerHour:
					return TerabytesPerHours;
				case DataTransferRateType.TerabytesPerDay:
					return TerabytesPerDays;
				case DataTransferRateType.TerabytesPerWeek:
					return TerabytesPerWeeks;
				case DataTransferRateType.TerabytesPerMonth:
					return TerabytesPerMonths;
				case DataTransferRateType.TerabytesPerYear:
					return TerabytesPerYears;
				case DataTransferRateType.TerabytesPerDecade:
					return TerabytesPerDecades;
				case DataTransferRateType.TerabytesPerCentury:
					return TerabytesPerCenturies;
				case DataTransferRateType.PetabytesPerNanosecond:
					return PetabytesPerNanoseconds;
				case DataTransferRateType.PetabytesPerMicrosecond:
					return PetabytesPerMicroseconds;
				case DataTransferRateType.PetabytesPerMillisecond:
					return PetabytesPerMilliseconds;
				case DataTransferRateType.PetabytesPerSecond:
					return PetabytesPerSeconds;
				case DataTransferRateType.PetabytesPerMinute:
					return PetabytesPerMinutes;
				case DataTransferRateType.PetabytesPerHour:
					return PetabytesPerHours;
				case DataTransferRateType.PetabytesPerDay:
					return PetabytesPerDays;
				case DataTransferRateType.PetabytesPerWeek:
					return PetabytesPerWeeks;
				case DataTransferRateType.PetabytesPerMonth:
					return PetabytesPerMonths;
				case DataTransferRateType.PetabytesPerYear:
					return PetabytesPerYears;
				case DataTransferRateType.PetabytesPerDecade:
					return PetabytesPerDecades;
				case DataTransferRateType.PetabytesPerCentury:
					return PetabytesPerCenturies;
				case DataTransferRateType.ExabytesPerNanosecond:
					return ExabytesPerNanoseconds;
				case DataTransferRateType.ExabytesPerMicrosecond:
					return ExabytesPerMicroseconds;
				case DataTransferRateType.ExabytesPerMillisecond:
					return ExabytesPerMilliseconds;
				case DataTransferRateType.ExabytesPerSecond:
					return ExabytesPerSeconds;
				case DataTransferRateType.ExabytesPerMinute:
					return ExabytesPerMinutes;
				case DataTransferRateType.ExabytesPerHour:
					return ExabytesPerHours;
				case DataTransferRateType.ExabytesPerDay:
					return ExabytesPerDays;
				case DataTransferRateType.ExabytesPerWeek:
					return ExabytesPerWeeks;
				case DataTransferRateType.ExabytesPerMonth:
					return ExabytesPerMonths;
				case DataTransferRateType.ExabytesPerYear:
					return ExabytesPerYears;
				case DataTransferRateType.ExabytesPerDecade:
					return ExabytesPerDecades;
				case DataTransferRateType.ExabytesPerCentury:
					return ExabytesPerCenturies;
				case DataTransferRateType.ZettabytesPerNanosecond:
					return ZettabytesPerNanoseconds;
				case DataTransferRateType.ZettabytesPerMicrosecond:
					return ZettabytesPerMicroseconds;
				case DataTransferRateType.ZettabytesPerMillisecond:
					return ZettabytesPerMilliseconds;
				case DataTransferRateType.ZettabytesPerSecond:
					return ZettabytesPerSeconds;
				case DataTransferRateType.ZettabytesPerMinute:
					return ZettabytesPerMinutes;
				case DataTransferRateType.ZettabytesPerHour:
					return ZettabytesPerHours;
				case DataTransferRateType.ZettabytesPerDay:
					return ZettabytesPerDays;
				case DataTransferRateType.ZettabytesPerWeek:
					return ZettabytesPerWeeks;
				case DataTransferRateType.ZettabytesPerMonth:
					return ZettabytesPerMonths;
				case DataTransferRateType.ZettabytesPerYear:
					return ZettabytesPerYears;
				case DataTransferRateType.ZettabytesPerDecade:
					return ZettabytesPerDecades;
				case DataTransferRateType.ZettabytesPerCentury:
					return ZettabytesPerCenturies;
				case DataTransferRateType.YottabytesPerNanosecond:
					return YottabytesPerNanoseconds;
				case DataTransferRateType.YottabytesPerMicrosecond:
					return YottabytesPerMicroseconds;
				case DataTransferRateType.YottabytesPerMillisecond:
					return YottabytesPerMilliseconds;
				case DataTransferRateType.YottabytesPerSecond:
					return YottabytesPerSeconds;
				case DataTransferRateType.YottabytesPerMinute:
					return YottabytesPerMinutes;
				case DataTransferRateType.YottabytesPerHour:
					return YottabytesPerHours;
				case DataTransferRateType.YottabytesPerDay:
					return YottabytesPerDays;
				case DataTransferRateType.YottabytesPerWeek:
					return YottabytesPerWeeks;
				case DataTransferRateType.YottabytesPerMonth:
					return YottabytesPerMonths;
				case DataTransferRateType.YottabytesPerYear:
					return YottabytesPerYears;
				case DataTransferRateType.YottabytesPerDecade:
					return YottabytesPerDecades;
				case DataTransferRateType.YottabytesPerCentury:
					return YottabytesPerCenturies;
			}
			throw new Exception("Unknown DataTransferRateType");
		}
	}
}