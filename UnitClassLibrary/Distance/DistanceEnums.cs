﻿
#pragma warning disable 1591

namespace UnitClassLibrary
{
    /// <summary>
    /// Enum for specifying the type of unit a Distance is
    /// </summary>
    public enum DistanceType { Inch, Millimeter, Centimeter, Meter, Kilometer, ThirtySecond, Sixteenth, Foot, Yard, Mile }
}
